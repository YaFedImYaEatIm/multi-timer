package yafedimyaeatim.apps.timer.core; /**
 * Created by Gondr on 13/10/2016.
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class TimerApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        FXMLLoader loader = new FXMLLoader();
        Parent parent = null;
        try {
            System.out.println(getClass().getClassLoader().getResource("mainApp.fxml"));
            parent = loader.load(getClass().getClassLoader().getResource("mainApp.fxml"));
        } catch (IOException e) {
            System.out.println("Missing mainApp.fxml File. Application will now exit");
            System.exit(1);
        }
        Scene primaryScene = new Scene(parent, 600, 400);

        primaryStage.setTitle("Multi-Timer App");
        primaryStage.setScene(primaryScene);
        primaryStage.show();

    }

    @Override
    public void stop(){
        System.exit(0);
    }

}
