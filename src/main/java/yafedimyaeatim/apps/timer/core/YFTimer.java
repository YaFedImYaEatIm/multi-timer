package yafedimyaeatim.apps.timer.core;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleStringProperty;
import javafx.util.Duration;

/**
 * Created by Gondr on 13/10/2016.
 */
public class YFTimer {

    private String name;

    private int hours = 0;
    private int minutes = 0;
    private int seconds = 0;

    private int curHours = 0;
    private int curMins = 0;
    private int curSecs = 0;

    private Timeline timer;
    SimpleStringProperty time = new SimpleStringProperty();

    private boolean running = false;

    /**
     * Special timer function of YAFED :)
     */
    public YFTimer(String name, int hours, int minutes, int seconds){
        this.name = name;
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
        this.curHours = hours;
        this.curMins = minutes;
        this.curSecs = seconds;
        timer = new Timeline(new KeyFrame(
                Duration.millis(1000),
                ae -> startTimer()
        ));
        timer.setCycleCount(Timeline.INDEFINITE);
    }

    public boolean toggleRunning(){
        System.out.println("Timer " + name + " Running State Toggled");
        if (running){
            timer.stop();
        }else if (!running && (hours > 0 || minutes > 0 || seconds > 0)){
            timer.play();
        }
        running = !running;
        return running;
    }

    public void startTimer(){
        if (!decrementSeconds()){
            timer.stop();
        }
        System.out.println(getTime());
    }

    public void reset(){
        if (running){
            timer.stop();
            running = false;
        }
        curHours = hours;
        curMins = minutes;
        curSecs = seconds;
        getTime();
    }

    private boolean decrementSeconds(){
        if (curSecs > 0){
            curSecs --;
            return true;
        }else if (decrementMinutes()){
            curSecs = 59;
            return true;
        }else{
            return false;
        }
    }

    private boolean decrementMinutes(){
        if (curMins > 0){
            curMins --;
            return true;
        }else if (decrementHours()){
            curMins = 59;
            return true;
        }else{
            return false;
        }
    }

    private boolean decrementHours(){
        if (curHours > 0){
            curHours --;
            return true;
        }else{
            return false;
        }
    }

    public SimpleStringProperty getTime(){
        time.setValue(curHours + ":" + curMins + ":" + curSecs);
        return time;
    }

}
