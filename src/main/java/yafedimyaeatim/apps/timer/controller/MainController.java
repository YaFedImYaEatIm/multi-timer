package yafedimyaeatim.apps.timer.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import yafedimyaeatim.apps.timer.core.YFTimer;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Created by Gondr on 13/10/2016.
 */
public class MainController implements Initializable {

    @FXML
    GridPane timerGrid;
    ObservableList<YFTimer> timers;
    ArrayList<YFTimer> yfTimers = new ArrayList<YFTimer>();
    ArrayList<Button> toggleTimers = new ArrayList<>();

    /**
     * Makes a Popup Stage where to add a Timer
     */
    public void addTimerPopUp(){
        final Stage popUpStage=  new Stage();
        final GridPane popUpParent = new GridPane();
        popUpParent.setVgap(10);
        popUpParent.setHgap(10);
        popUpParent.setPadding(new Insets(10, 10, 10, 10));
        //make column 1 and 2 equal
        ColumnConstraints colConstraints1 = new ColumnConstraints();
        colConstraints1.setPercentWidth(50);
        ColumnConstraints colConstraints2 = new ColumnConstraints();
        colConstraints2.setPercentWidth(50);
        popUpParent.getColumnConstraints().addAll(colConstraints1, colConstraints2);
        popUpParent.setPrefSize(300, 200);
        /*Label Name*/
        Label lblName = new Label("Timer Name:");
        lblName.setAlignment(Pos.CENTER);
        lblName.setFont(Font.font("System", FontWeight.BOLD, 12));
        popUpParent.add(lblName, 0, 0);
        /*Timer Name Text Field*/
        final TextField tfName = new TextField();
        tfName.setPromptText("Timer Name");
        popUpParent.add(tfName, 1, 0);

        /*Lable Hours*/
        Label lblHours = new Label("Timer Hours:");
        lblHours.setAlignment(Pos.CENTER);
        lblHours.setFont(Font.font("System", FontWeight.BOLD, 12));
        popUpParent.add(lblHours, 0, 1);
        /*Timer Name Text Field*/
        final TextField tfHours = new TextField();
        tfHours.setPromptText("Timer Hours");
        popUpParent.add(tfHours, 1, 1);

        /*Lable Hours*/
        Label lblMins = new Label("Timer Minutes:");
        lblMins.setAlignment(Pos.CENTER);
        lblMins.setFont(Font.font("System", FontWeight.BOLD, 12));
        popUpParent.add(lblMins, 0, 2);
        /*Timer Name Text Field*/
        final TextField tfMins = new TextField();
        tfMins.setPromptText("Timer Minutes");
        popUpParent.add(tfMins, 1, 2);

        /*Lable Hours*/
        Label lblSecs = new Label("Timer Seconds:");
        lblSecs.setAlignment(Pos.CENTER);
        lblSecs.setFont(Font.font("System", FontWeight.BOLD, 12));
        popUpParent.add(lblSecs, 0, 3);
        /*Timer Name Text Field*/
        final TextField tfSecs = new TextField();
        tfSecs.setPromptText("Timer Seconds");
        popUpParent.add(tfSecs, 1, 3);

        /*Button that adds a timer to the timers*/
        Button addTimerBtn = new Button("Add Timer");
        toggleTimers.add(addTimerBtn);
        addTimerBtn.addEventHandler(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
                    public void handle(MouseEvent e) {
                        try {

                            int hours = 0;
                            if (!tfHours.getText().equals("") && tfHours.getText() != null){
                                hours = Integer.parseInt(tfHours.getText());
                            }
                            int mins = 0;
                            if (!tfMins.getText().equals("") && tfMins.getText() != null){
                                mins = Integer.parseInt(tfMins.getText());
                            }
                            int secs = 0;
                            if (!tfSecs.getText().equals("") && tfSecs.getText() != null){
                                secs = Integer.parseInt(tfSecs.getText());
                            }
                            if (hours >= 0 && hours <= 24 && mins >= 0 && mins <= 60 && secs >= 0 && secs <= 60 &&
                                    tfName.getText().length() != 0) {
                                addYFTimer(tfName.getText(), hours, mins, secs);
                                popUpStage.close();
                            }
                        } catch(NumberFormatException e1){
                            System.out.println("Value is not formattable.");
                        }
                    }
                });

        popUpParent.add(addTimerBtn, 0, 4);
        Scene scene = new Scene(popUpParent, 300, 200);
        popUpStage.setScene(scene);
        popUpStage.show();
    }

    /**
     * Resets all the timers in the application.
     */
    public void resetAllTimers(){
        for (YFTimer yfTimer: yfTimers){
            yfTimer.reset();
        }
        for (Button btn: toggleTimers){
            btn.setText("Start");
        }
    }

    /**
     * Addes a Yafed Timer to the application.
     * @param name
     * @param hours
     * @param mins
     * @param secs
     */
    public void addYFTimer(String name, int hours, int mins, int secs){
        final YFTimer yfTimer = new YFTimer(name, hours, mins, secs);
        timerGrid.addRow(yfTimers.size());
        /*Timer Label*/
        Label label = new Label(name + " " + hours + "h " + mins + "m " + secs + "s");
        timerGrid.add(label,0, yfTimers.size());
        /*Add Label for Current timer*/
        Label label1 = new Label();
        label1.textProperty().bind(yfTimer.getTime());
        label1.setAlignment(Pos.CENTER);
        timerGrid.add(label1, 1, yfTimers.size());
        /*Add Button which toggles start/pause*/
        Button toggleTimer = new Button("Start");
        toggleTimer.setAlignment(Pos.CENTER_RIGHT);
        toggleTimer.addEventHandler(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
                    public void handle(MouseEvent e) {
                        if (yfTimer.toggleRunning()){
                            toggleTimer.setText("Pause");
                        }else{
                            toggleTimer.setText("Start");
                        }
                    }
                });
        timerGrid.add(toggleTimer, 2, yfTimers.size());
        /*Adds a Reset button*/
        Button resetTimer = new Button("Reset");
        resetTimer.setAlignment(Pos.CENTER_RIGHT);
        resetTimer.addEventHandler(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
                    public void handle(MouseEvent e) {
                        yfTimer.reset();
                        toggleTimer.setText("Start");
                    }
                });
        timerGrid.add(resetTimer, 3, yfTimers.size());
        yfTimers.add(yfTimer);
        timers = FXCollections.observableArrayList(yfTimers);
    }

    public void initialize(URL location, ResourceBundle resources) {

    }
}
